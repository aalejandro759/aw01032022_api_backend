//Requiring Modules
const express = require('express');
const app = express();
app.use(express.json());
const mongoose = require("mongoose");
var mockEnvironment = require('../src/environments/environments');
const mongoURI  =`mongodb+srv://${mockEnvironment.userMongo}:${mockEnvironment.passwordMongo}@kushkitestcluster.gvdmg.mongodb.net/${mockEnvironment.nameDb}?retryWrites=true&w=majority`;

//Middle-ware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function (_req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization, limit, offset"
    );
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "*");
    next();
});

//settings 
app.set('port', process.env.PORT || 32300);

//dbConnection
mongoose.connect(mongoURI).
    then(resp =>{
        console.log('dbMongo')
});

//const routes
const gameRoute = require('./routes/game');

//routes
app.use(gameRoute);

app.listen(app.get('port'), () => {
    console.log('App listening on port', app.get('port'))
});
