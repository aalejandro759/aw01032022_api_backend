const router = require('express').Router();
const express = require('express');
const jwt = require('jsonwebtoken')
const app = express();
app.use(express.json());
const UserModel = require('./../models/userModel');
const bcrypt = require('bcryptjs');
var mockEnvironment = require('../environments/environments');

const isAuth = (req, res, next) => {
    var token = req.headers.authorization;
    if (token && token.split(" ")[0] === "Bearer") {
        jwt.verify(token.split(" ")[1], mockEnvironment.seed, (err, decoded) => {
            if (err) {
                return res.status(401).json({ message: "Token Invalid" });
            }
            req.user = decoded.user;
            next();
        });
    } else {
        return res.status(401).json({ message: "Token not found" });
    }
}
app.use(function (_req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization, limit, offset"
    );
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "*");
    next();
});

const userRegisterSuccessResponse = {
    message: 'Account created successfully',
    redirectTo: 'login'
}

const pswIncorrectResponse = {
    message: 'Invalid username or password',
    redirectTo: 'login'
}

router.post('/api/register', async (req, res) => {
    const { userName, password, email } = req.body;
    let isRegisterUser = await UserModel.findOne({ email });
    if (isRegisterUser) {
        return res.status(204).json();
    }
    const hashedPsw = await bcrypt.hash(password, 12);
    let user = new UserModel({
        userName,
        email,
        password: hashedPsw
    });
    await user.save();
    res.status(200).json(userRegisterSuccessResponse);
});

router.post('/api/login', async (req, res) => {
    const { email, password } = req.body;
    const user = await UserModel.findOne({ email });
    if (!user) {
        return res.status(204).json();
    }
    const pswIsMatch = await bcrypt.compare(password, user.password);
    if (!pswIsMatch) {
        return res.status(403).json(pswIncorrectResponse);
    }
    let userJwt = {
        email: user.email,
        _id: user._id,
        totalCredits: user.totalCredits
    }
    res.status(200).json({
        totalCredits: user.totalCredits,
        redirectTo: 'game',
        jwt: generateJwtToken(userJwt)
    });
});

router.post('/api/game', isAuth, async (req, res) => {
    let userBody = req.user;
    const _id = userBody._id;
    const userDb = await UserModel.findOne({ _id });
    if (userBody.totalCredits < 40) {
        await firstConditional(_id, userDb, req, res);
    }
    if (userBody.totalCredits >= 40 && userBody.totalCredits <= 60) {
        await secondConditional(_id, userDb, req, res);
    }
    if (userBody.totalCredits > 60) {
        await thirdConditional(_id, userDb, req, res);
    }
});

function randomResponse() {
    const playGame = ['C', 'L', 'O', 'W'];
    let rValue = new Array();
    for (let i = 0; i < playGame.length - 1; i++) {
        const randResult = Math.floor(Math.random() * playGame.length);
        rValue.push(playGame[randResult]);
    }
    return rValue;
}

function checkIfDuplicateExists(arr) {
    let uniqueChars = [...new Set(arr)];
    return uniqueChars;
}

function repeatGame30(min, max) {
    const probabilityRepeat = Math.floor(Math.random() * (max - min + 1) + min);
    let userWinAgain = [];
    if (probabilityRepeat >= 0 && probabilityRepeat <= 30) {
        const resultNewRandom = randomResponse();
        userWinAgain = checkIfDuplicateExists(resultNewRandom)
        return {userWinAgain, resultNewRandom};
    } else {
        return {userWinAgain, resultNewRandom:[]};
    }
}

function repeatGame60(min, max) {
    const probabilityRepeat = Math.floor(Math.random() * (max - min + 1) + min);
    let userWinAgain = [];
    if (probabilityRepeat >= 0 && probabilityRepeat <= 60) {
        const resultNewRandom = randomResponse();
        userWinAgain = checkIfDuplicateExists(resultNewRandom)
        return {userWinAgain, resultNewRandom};
    } else {
        return {userWinAgain, resultNewRandom:[]};;
    }
}

function switchTotalPay(value) {
    switch (value) {
        case 'C':
            return 10;
        case 'L':
            return 20;
        case 'O':
            return 30;
        case 'W':
            return 40;
        default:
    }
}
function generateJwtToken(user) {
    return jwt.sign(
        {
            user
        },
        mockEnvironment.seed,
        { expiresIn: mockEnvironment.expire_token }
    );
}

async function firstConditional(_id, userDb, req, res) {
    const result = randomResponse();
    const userWin = checkIfDuplicateExists(result);
    if (userWin.length === 1) {
        userDb.totalCredits += switchTotalPay(userWin[0]);
        await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone();
        const userDbUpdateCredits = await UserModel.findOne({ _id });
        let userJwtCredits = {
            email: userDbUpdateCredits.email,
            _id: userDbUpdateCredits._id,
            totalCredits: userDbUpdateCredits.totalCredits
        }
        res.status(200).json({
            totalCredits: userDbUpdateCredits.totalCredits,
            resultGame: result,
            jwt: generateJwtToken(userJwtCredits)
        });
    }
    else {
        await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone()
        const userDbUpdate = await UserModel.findOne({ _id });
        let userJwt = {
            email: userDbUpdate.email,
            _id: userDbUpdate._id,
            totalCredits: userDbUpdate.totalCredits
        }
        res.status(200).json({
            totalCredits: userDbUpdate.totalCredits,
            resultGame: result,
            jwt: generateJwtToken(userJwt)
        });
    }
}

async function secondConditional(_id, userDb, req, res) {
    const result = randomResponse();
    const userWin = checkIfDuplicateExists(result);
    if (userWin.length === 1) {
        let repeatGameResult = repeatGame30(0, 100);
        if (repeatGameResult.userWinAgain.length === 1) {
            userDb.totalCredits += switchTotalPay(repeatGameResult.userWinAgain[0]);
            await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone();
            const userDbUpdateWin = await UserModel.findOne({ _id });
            let userJwtWin = {
                email: userDbUpdateWin.email,
                _id: userDbUpdateWin._id,
                totalCredits: userDbUpdateWin.totalCredits
            }
            res.status(200).json({
                totalCredits: userDbUpdateWin.totalCredits,
                resultGame: repeatGameResult.resultNewRandom,
                jwt: generateJwtToken(userJwtWin)
            });
        }
        else {
            userDb.totalCredits += switchTotalPay(userWin[0]);
            await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone()
            const userDbUpdate = await UserModel.findOne({ _id });
            let userJwt = {
                email: userDbUpdate.email,
                _id: userDbUpdate._id,
                totalCredits: userDbUpdate.totalCredits
            }
            res.status(200).json({
                totalCredits: userDbUpdate.totalCredits,
                resultGame: result,
                jwt: generateJwtToken(userJwt)
            });
        }
    }
    else {
        await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone()
        const userDbUpdate = await UserModel.findOne({ _id });
        let userJwt = {
            email: userDbUpdate.email,
            _id: userDbUpdate._id,
            totalCredits: userDbUpdate.totalCredits
        }
        res.status(200).json({
            totalCredits: userDbUpdate.totalCredits,
            resultGame: result,
            jwt: generateJwtToken(userJwt)
        });
    }
}

async function thirdConditional(_id, userDb, req, res) {
    const result = randomResponse();
    const userWin = checkIfDuplicateExists(result);
    if (userWin.length === 1) {
        let repeatGameResult = repeatGame60(0, 100);
        if (repeatGameResult.userWinAgain.length === 1) {
            userDb.totalCredits += switchTotalPay(repeatGameResult.userWinAgain[0]);
            await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone();
            const userDbUpdate60 = await UserModel.findOne({ _id });
            let userJwt60 = {
                email: userDbUpdate60.email,
                _id: userDbUpdate60._id,
                totalCredits: userDbUpdate60.totalCredits
            }
            res.status(200).json({
                totalCredits: userDbUpdate60.totalCredits,
                resultGame: repeatGameResult.resultNewRandom,
                jwt: generateJwtToken(userJwt60)
            });
        }
        else {
            userDb.totalCredits += switchTotalPay(userWin[0]);
            await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone();
            const userDbUpdate = await UserModel.findOne({ _id });
            let userJwt = {
                email: userDbUpdate.email,
                _id: userDbUpdate._id,
                totalCredits: userDbUpdate.totalCredits
            }
            res.status(200).json({
                totalCredits: userDbUpdate.totalCredits,
                resultGame: result,
                jwt: generateJwtToken(userJwt)
            });
        }
    }
    else {
        await UserModel.findByIdAndUpdate(_id, { totalCredits: userDb.totalCredits - 1 }).clone();
        const userDbUpdate = await UserModel.findOne({ _id });
        let userJwt = {
            email: userDbUpdate.email,
            _id: userDbUpdate._id,
            totalCredits: userDbUpdate.totalCredits
        }
        res.status(200).json({
            totalCredits: userDbUpdate.totalCredits,
            resultGame: result,
            jwt: generateJwtToken(userJwt)
        });
    }
}
module.exports = router; 